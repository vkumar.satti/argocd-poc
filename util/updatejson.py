import argparse
import datetime
import json
import yaml
import os
import pprint

from argparse import RawTextHelpFormatter
from os import listdir
from os.path import isfile, join
from subprocess import Popen, PIPE

def get_args():
    parser = argparse.ArgumentParser(formatter_class=RawTextHelpFormatter)
    parser.add_argument('-newversion', dest='version', help='Update the version of a specific application.')
    return parser.parse_args()

def update_helm_version_file(newversion):
    with open('./chart/sample-app/Chart.yaml', 'r+') as f:
        data = yaml.load(f ,  Loader=yaml.FullLoader)
        # data = json.load(f)
        data['version'] = newversion
        f.seek(0)        # <--- should reset file position to the beginning.
        yaml.dump(data, f, indent=4)
        f.truncate()
        print(data)

def main():
    args = get_args()
    print("1.updated successfully.")
    print(args.version)
    update_helm_version_file(args.version)
    print("2.updated successfully.")


if __name__ == '__main__':
    main()

